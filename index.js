const express = require('express')
const mongoose = require('mongoose')

const dotenv = require('dotenv')
dotenv.config()

const app = express()
const PORT = 3001

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))

//1

    const userSchema = new mongoose.Schema({
        userName: {
            type: String,
            required: [true, `Userame is required`]
        },
        password: {
            type: String,
            required: [true, `Password is required`]
        }
    })

//2
const User = mongoose.model(`User`, userSchema)

//3
app.post('/signup', (req, res) => {
    //console.log("test");

    User.findOne({userName: req.body.userName}).then((user, err) => {
        if(user != null && user.userName == req.body.userName){
			return res.status(500).send(`User already exist!`)
		}
        else {
            let newUser = new User({
				userName: req.body.userName,
                password: req.body.password
			})

            newUser.save().then((saveUser, err) => {
               // console.log(saveUser)
                if(saveUser){
                    return res.status(200).send(`New user registered!`)
                }
                else {
                    return res.status(500).send(err)
                }
            })
        }

    })

})

//v2
// app.post('/signup', (req, res) => {
//     //console.log("test");

//             let newUser = new User({
// 				userName: req.body.userName,
//                 password: req.body.password
// 			})

//             newUser.save().then((saveUser, err) => {
//                 console.log(saveUser)
//                 if(saveUser){
//                     return res.status(200).send(`New user registered!`)
//                 }
//                 else {
//                     return res.status(500).send(err)
//                 }
//             })

// })

app.listen(PORT, () => console.log(`Server is connected at port ${PORT}`))